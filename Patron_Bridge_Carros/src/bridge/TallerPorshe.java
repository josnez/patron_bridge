package bridge;

public class TallerPorshe extends Taller {

	public TallerPorshe(IAdornoDeCapo adorno) {
		super(adorno);
		System.out.println("Taller de Porshe\n");
	}

}
